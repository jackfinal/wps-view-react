import React from "react"
import {Modal, Progress} from 'antd';

export default class MyProgress extends React.Component{
    constructor(props) {
        super(props);
        this.state={}
    }

    render() {
        return <div>
            <Modal
                style={{marginTop:'8%'}}
                maskClosable={false}
                closable={false}
                footer={null}
                visible={this.props.visible}
                title={this.props.fileName}
            >
                <Progress
                    strokeColor={{
                        from: '#108ee9',
                        to: '#87d068',
                    }}
                    percent={(Number(this.props.percent)).toFixed(2)}
                    status="active"
                />
            </Modal>
        </div>
    }
}
